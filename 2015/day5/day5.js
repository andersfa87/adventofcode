var fs = require('fs'),
	_ = require('lodash');

var vowels = ['a','e','i','o','u'];
var not_allowed = ['ab', 'cd', 'pq', 'xy'];

console.time("duration");

fs.readFile('input', 'utf8', function(err, data){
	if(err){
		console.error(err);
		return;
	}
	
	
	var list = data.split('\n');
	
	var counter = 0
	
	var letterRegex = /(.).\1/;
	
	var pairRegex = /(.{2}).*\1/;
	
	for(var i = 0; i < list.length; i++){
		var text = list[i];
		
		if(letterRegex.test(text) && pairRegex.test(text)){
			counter++;
		} 
		
	}
	
	console.log(counter);
	
	console.timeEnd("duration");
	
});

function part1(list){
	var nice = 0;
	
	for(var i = 0; i < list.length; i++){
		var text = list[i];
		
		var previousChar = '';
		var vowelCount = 0;
		var isNice = true;
		var hasSameTwoLettersInARow = false;
		for(var y = 0; y < text.length; y++){
			var currentChar = text[y];
			
			if(textContainsFromList(currentChar, vowels)){
				vowelCount ++;
			}
			
			if(textContainsFromList(previousChar + currentChar, not_allowed)){
				isNice = false;
			} else if(previousChar == currentChar){
				hasSameTwoLettersInARow = true;
			}
			
			previousChar = currentChar;
		}		
		
		if(vowelCount >= 3 && isNice && hasSameTwoLettersInARow){
			nice ++;
		}
		
	}
	
	console.log(nice);
}

function textContainsFromList(text, list){
	return _.some(list, function(val){
		return text.indexOf(val) !== -1;
	});
}