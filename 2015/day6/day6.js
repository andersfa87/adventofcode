var fs = require('fs'),
	_ = require('lodash');

var TURN_ON = 'turn on',
	TURN_OFF = 'turn off',
	TOGGLE = 'toggle';

console.time("duration");

fs.readFile('input', 'utf8', function(err, data){
	if(err){
		console.error(err);
		return;
	}
	
	part2(data);
	
	console.timeEnd("duration");
	
});

function part2(data){
	var list = data.split('\n');
	
	var regex = /(turn off|toggle|turn on) (\d+),(\d+) through (\d+),(\d+)/;
	
	var grid = {};
	
	for(var i = 0; i < list.length; i++){
		var instruction = list[i];
		var matches = instruction.match(regex);
		
		var action = matches[1],
			fromX = parseInt(matches[2]),
			fromY = parseInt(matches[3]),
			toX = parseInt(matches[4]),
			toY = parseInt(matches[5]);
		
		for(var x = fromX; x <= toX; x++){
			for(var y = fromY; y <= toY; y++){
				
				var light = grid[x+"x"+y];
				if(typeof light === 'undefined'){
					light = { brightness : 0 };
					grid[x+"x"+y] = light;
				} 
				
				switch(action){
					case TURN_ON:
						light.brightness++;
						break;
					case TURN_OFF:
						if(light.brightness > 0){
							light.brightness--;
						}
						break;
					case TOGGLE:
						light.brightness += 2;					
					break;
				}	
			}
		}
	}
	
	var keys = Object.keys(grid);
	console.log("Total lights: " + keys.length);
	
	var brightness = _.sum(keys, function(key){
		return grid[key].brightness;
	});
	
	console.log("Total brightness: " + brightness);
}

function part1(data){
	var list = data.split('\n');
	
	var regex = /(turn off|toggle|turn on) (\d+),(\d+) through (\d+),(\d+)/;
	
	var grid = {};
	
	for(var i = 0; i < list.length; i++){
		var instruction = list[i];
		var matches = instruction.match(regex);
		
		var action = matches[1],
			fromX = parseInt(matches[2]),
			fromY = parseInt(matches[3]),
			toX = parseInt(matches[4]),
			toY = parseInt(matches[5]);
		
		for(var x = fromX; x <= toX; x++){
			for(var y = fromY; y <= toY; y++){
				switch(action){
					case TURN_ON:
						grid[x+"x"+y] = true;
						break;
					case TURN_OFF:
						grid[x+"x"+y] = false;
						break;
					case TOGGLE:
						if(grid[x+"x"+y] === true){
							grid[x+"x"+y] = false;
						} else {
							grid[x+"x"+y] = true;
						}
					
					break;
				}	
			}
		}
	}
	
	var keys = Object.keys(grid);
	console.log("Total lights: " + keys.length);
	
	var lightsOn = _.filter(keys, function(key){
		return grid[key] === true;
	});
	
	console.log("Total lights on: " + lightsOn.length);
}