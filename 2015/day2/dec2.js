var file = require('fs'),
	_ = require('lodash');

function part1(){
	file.readFile('dec2input.data', 'utf8', function(err, data){
		var total = 0;
		var presentDimensions = data.split('\n');
		for(var i = 0; i < presentDimensions.length; i++){
			var dimensions = presentDimensions[i].split('x');
			
			var l = dimensions[0];
			var w = dimensions[1];
			var h = dimensions[2];
			
			var sides = [l*w, w*h, h*l];
					
			var area = _.sum(sides, function(side){
				return side * 2;
			});
			
			area += _.min(sides);
			
			total += area;	
		}	
		console.log(total);
	});
}

function part2(){
	file.readFile('dec2input.data', 'utf8', function(err, data){
		var total = 0;
		var presentDimensions = data.split('\n');
		for(var i = 0; i < presentDimensions.length; i++){
			var dimensions = presentDimensions[i].split('x');
			
			var l = parseInt(dimensions[0]);
			var w = parseInt(dimensions[1]);
			var h = parseInt(dimensions[2]);
			
			var n = [l,w,h];
			
			var sum = _.sum(n) * 2;
			
			var max = _.max(n);
			
			sum -= (2*max);
						
			var bow = (l*w*h);
										
			total += sum + bow;	
		}	
		console.log(total);
	});
}

part2();