var fs = require('fs'),
	_ = require('lodash');

var WEST = '<',
	EAST = '>',
	NORTH = '^',
	SOUTH = 'v';

fs.readFile('input', 'utf8', function(err, data){
	if(err){
		console.error(err);
		return;
	}
	
	part2(data);
	
});

function part2(data){
	
	function Santa(grid){
		var self = this;
		this.x = 0;
		this.y = 0;
		this.houses = 0;
		this.makeMove = function(move){
			switch (data[i]) {
				case WEST:
					self.x--;	
					break;		
				case EAST:
					self.x++;
					break;			
				case NORTH:
					self.y++;
					break;				
				case SOUTH:
					self.y--;
					break;
			}
			if(typeof grid[self.x + "x" + self.y] === 'undefined'){
				grid[self.x + "x" + self.y] = "santa was here";
				self.houses ++;
			}
		}
	}
	var grid = {};
	var santa = new Santa(grid);
	var robo = new Santa(grid);
	
	var turn = santa;
	
	for(var i = 0; i < data.length; i++){
		turn.makeMove(data[i]);
		if(turn == santa){
			turn = robo;
		} else {
			turn = santa;
		}
	}
	
	console.log(santa.houses + robo.houses);
	console.log(Object.keys(grid).length);
	
}

function part1(data){
	//data = '^v^v^v^v^v';
	
	var totalHouses = 1;
	
	var grid = {};
	
	var x = 0,
		y = 0;
	
	grid[x + "x" + y] = 1;
	
	for(var i = 0; i < data.length; i++){
		
		switch (data[i]) {
			case WEST:
				x--;	
				break;		
			case EAST:
				x++;
				break;			
			case NORTH:
				y++;
				break;				
			case SOUTH:
				y--
				break;
		}
		if(typeof grid[x + "x" + y] != 'undefined'){
			var presents = grid[x + "x" + y];
			grid[x + "x" + y] = presents + 1;
		} else {
			grid[x + "x" + y] = 1;
			totalHouses ++;
		}
		
	}
	
	console.log("totalHouses:" + totalHouses);
}