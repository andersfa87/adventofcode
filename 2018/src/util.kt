import java.io.File

fun readFile(fileName: String) : String {
    File(fileName).inputStream().use {
        val inputString = it.bufferedReader().use { it.readText() }
        return inputString.replace("\r", "")
    }
}

fun manhattanDistance(x1: Int, y1: Int, x2: Int, y2: Int) : Int {
    return (Math.abs(x1 - x2) + Math.abs(y1 - y2))
}

class Point(val x: Int, val y: Int) {
    override fun equals(other: Any?): Boolean {
        other?.let {
            if(other is Point) {
                return this.x == other.x && this.y == other.y
            }
        }
        return false
    }

    fun distance(other: Point) : Int {
        return (Math.abs(this.x - other.x) + Math.abs(this.y - other.y))
    }
}