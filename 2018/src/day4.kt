
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

fun main(args: Array<String>) {

    val dateFormatter = SimpleDateFormat("y-M-d H:m")

    val entries = readFile("day4input.txt")
            .split("\n")
            .map {
                val timestamp = Calendar.getInstance()
                timestamp.time = dateFormatter.parse(it.substring(1, it.indexOf("]")))
                Entry(
                    timestamp,
                    it.substring(it.indexOf("]") + 2)
                )
            }
            .sortedBy { it.timestamp }

    val regex = Regex("#([0-9]+)")
    val guards = HashMap<Int, Guard>()

    var currentGuard : Guard? = null
    for(entry in entries) {
        when {
            entry.command.startsWith("Guard") -> {
                val matches = regex.find(entry.command)
                val id = matches!!.groups[1]!!.value.toInt()

                val guard = guards.getOrDefault(id, Guard(id))

                guards[id] = guard
                currentGuard = guard
            }
            entry.command == "falls asleep" -> {

            }
            entry.command == "wakes up" -> {
                val fellAsleep = entries[entries.indexOf(entry) - 1].timestamp
                currentGuard!!.sleepIntervals.add(SleepInterval(fellAsleep, entry.timestamp))
            }
        }
    }

    guards.values.forEach { it.generateMinuteStats() }
    val guard = guards.values.maxBy { it.totalTimeSlept() }

    guard?.let { guard ->
        println("Guard: ${guard.id} slept the most")
        val minuteSleptMost = guard.minuteSleptMost()
        println("The minute slept most was: ${minuteSleptMost.minute}, with count: ${minuteSleptMost.count}")
        println("Answer is: ${guard.id * minuteSleptMost.minute}")


        val guard2 = guards.values.filter { it.stats.size > 0 }.maxBy { maxBy ->
            maxBy.stats.maxBy { it.count }!!.count
        }

        guard2?.let {
            val minute = it.minuteSleptMost()
            println("Guard: ${guard2.id} slept ${minute.count} times on minute:${minute.minute}")
            println("Result: ${guard2.id * minute.minute}")
        }

    }

}

class Guard(val id: Int) {
    val sleepIntervals = ArrayList<SleepInterval>()
    val stats = ArrayList<MinuteStat>()

    fun totalTimeSlept(): Int {
        return sleepIntervals.sumBy {
            TimeUnit.MINUTES.convert(it.end.timeInMillis - it.start.timeInMillis, TimeUnit.MILLISECONDS).toInt()
        }
    }

    fun generateMinuteStats() {
        for (interval in sleepIntervals) {
            for(minute in interval.start.get(Calendar.MINUTE) until interval.end.get(Calendar.MINUTE)) {
                val stat = this.stats.singleOrNull { it.minute == minute }
                stat?.let {
                    stat.count++
                } ?: run {
                    this.stats.add(MinuteStat(minute, 1))
                }
            }
        }
    }

    fun minuteSleptMost(): MinuteStat {
        return this.stats.sortedByDescending { it.count }.first()
    }
}

class MinuteStat(val minute: Int, var count: Int)

class SleepInterval(val start: Calendar, val end: Calendar)

class Entry(val timestamp: Calendar, val command: String)

