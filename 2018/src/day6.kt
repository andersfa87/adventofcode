fun main(args: Array<String>) {

    val data = readFile("day6input.txt")
    //val data = "1, 1\n" +
            "1, 6\n" +
            "8, 3\n" +
            "3, 4\n" +
            "5, 5\n" +
            "8, 9"
    val coordinates = data.split("\n").map { line ->
        val coordinatesArray = line.split(", ")
        Point(coordinatesArray[0].toInt(), coordinatesArray[1].toInt())
    }.map { Region(it) }

    println("${coordinates.size}")

    //most topleft
    val startX = coordinates.map { it.dangerousCoordinate.x }.min()!!
    val startY = coordinates.map { it.dangerousCoordinate.y }.min()!!
    val endX = coordinates.map { it.dangerousCoordinate.x }.max()!!
    val endY = coordinates.map { it.dangerousCoordinate.y }.max()!!

    val grid = HashMap<Point, ArrayList<Region>>()

    for(x in startX..endX) {
        for(y in  startY..endY) {
            val point = Point(x,y)
            val closestCoordinates = coordinates.sortedBy { it.dangerousCoordinate.distance(point) }
            val distance = point.distance(closestCoordinates[0].dangerousCoordinate)
            val list = grid.getOrDefault(point, ArrayList())

            for(coordinate in closestCoordinates) {
                if(coordinate.dangerousCoordinate.distance(point) == distance) {
                    coordinate.points.add(point)
                    list.add(coordinate)
                }
                else {
                    break
                }
            }

            grid[point] = list
        }
    }

    val size = grid.values.filter { it.size == 1 }.groupBy { it }.map { it.value.size }.sortedDescending().first()
    println("Part1 $size")

    val points = grid.keys.filter { point ->
        val sum = coordinates.sumBy { it.dangerousCoordinate.distance(point) }
        sum < 10000
    }

    println("Part2: ${points.size}")




}

class Region(val dangerousCoordinate: Point) {
    val points = ArrayList<Point>()
}