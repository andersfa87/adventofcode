

fun main(args: Array<String>) {
    val data = readFile("day2input.txt")

    val lines = data.split("\n")

    val result = lines.map { it.toCharArray().groupBy { it }.mapValues { it.value.size } }

    val twiceCount = result.count { it.containsValue(2) }
    val trippleCount = result.count { it.containsValue(3) }

    println("TwiceCount: ${twiceCount}")
    println("TripleCount: ${trippleCount}")
    println("Checksum: ${twiceCount*trippleCount}")

    for(i in 0 until lines.size) {
        val line = lines[i]

        for(j in i+1 until lines.size-1) {
            val compareLine = lines[j]

            var diffcount = 0

            for(x in 0 until compareLine.length) {
                if(line[x] != compareLine[x]){
                    diffcount++
                }
                if(diffcount > 1) {
                    break
                }
            }

            if(diffcount == 1) {
                return println("Found result: ${line} and ${compareLine}")
            }
        }
    }
}