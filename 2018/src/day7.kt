fun main(args: Array<String>) {
    val data = readFile("day7input.txt")
//    val data = "Step C must be finished before step A can begin.\n" +
//            "Step C must be finished before step F can begin.\n" +
//            "Step A must be finished before step B can begin.\n" +
//            "Step A must be finished before step D can begin.\n" +
//            "Step B must be finished before step E can begin.\n" +
//            "Step D must be finished before step E can begin.\n" +
//            "Step F must be finished before step E can begin."

    val regex = Regex("Step ([A-Z]) must be finished before step ([A-Z]) can begin.")

    val steps = HashMap<Char, Step>()

    data
        .split("\n")
        .forEach { line ->
            regex.find(line)?.let {
                val beforeName = it.groups[1]!!.value[0]
                val afterName = it.groups[2]!!.value[0]

                val beforeStep = steps.getOrDefault(beforeName, Step(beforeName))
                val afterStep = steps.getOrDefault(afterName, Step(afterName))

                beforeStep.afterSteps.add(afterStep)
                afterStep.beforeSteps.add(beforeStep)

                steps[beforeName] = beforeStep
                steps[afterName] = afterStep
            }
        }

    var result = ArrayList<Step>()

    while(result.size != steps.values.size) {
        val nextStep = steps.values.filter { f -> !result.contains(f) && f.beforeSteps.all { result.contains(it) } }.sortedBy { it.name }.first()
        result.add(nextStep)
    }

    println("First: ${String(result.map { it.name }.toCharArray())}")

    val num_workers = 5
    var time = 0
    result = ArrayList()
    val workers = (1..num_workers).map { Worker(it) }
    while(result.size != steps.values.size) {
        //starts jobs if worker is available and if step is available
        for (worker in workers.filter { it.currentJob == null }) {
            val step = steps.values.filter { f -> !workers.any { it.currentJob?.step == f } && !result.contains(f) && f.beforeSteps.all { result.contains(it) } }.sortedBy { it.name }.firstOrNull()
            step?.let {
                worker.currentJob = Job(it, it.name.toInt() - 'A'.toInt() + 1 + 60)
            }
        }

        //progress time
        workers.forEach {worker ->
            worker.currentJob?.let {
                it.timeRemaining--
            }
        }

        //job complete? add to result
        workers.filter { it.currentJob != null }.sortedBy { it.currentJob!!.step.name }.forEach { worker ->
            worker.currentJob?.let {
                if(it.timeRemaining == 0) {
                    result.add(it.step)
                    worker.currentJob = null
                }
            }
        }
        time++
    }

    println("Second: $time seconds")
}

class Step(val name: Char) {
    val beforeSteps = ArrayList<Step>()
    val afterSteps = ArrayList<Step>()
}

class Job(val step: Step, var timeRemaining: Int)

class Worker(val id: Int) {
    var currentJob: Job? = null
}