fun main(args: Array<String>) {
    var data = readFile("day5input.txt")

    val result = react(data)

    println("Length: ${result.length}")

    //data = "dabAcCaCBAcCcaDA"
    val units = data.groupBy { it.toLowerCase() }
    val variants = units.map {
        val new = data.replace(it.key.toString(), "", true)
        new
    }
    val polymer = variants.map { react(it) }
    val best = polymer.minBy { it.length }

    best?.let {
        println("Best: ${it.length}")
    }

}

fun react(data: String): String {
    var result = data
    var i = 0
    while(i < result.length - 1) {
        val first = result[i]
        val second = result[i+1]

        if (first.equals(second, true) && ((first.isUpperCase() && second.isLowerCase()) || (first.isLowerCase() && second.isUpperCase()))) {
            result = result.substring(0, i) + result.substring(i + 2)
            if(i > 0) {
                i--
            }
        } else {
            i++
        }
    }
    return result
}