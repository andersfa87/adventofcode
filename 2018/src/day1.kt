

fun main(args: Array<String>) {
    val data = readFile("day1input.txt")
    val numbers = data.split("\n").map { it.replace("+", "").trim() }.map { it.toInt() }

    println(numbers.sum())

    val seenFrequencies = HashSet<Int>();
    var frequency = 0;
    seenFrequencies.add(frequency)

    outer@ while(true) {
        inner@ for (frequencyChange in numbers) {
            frequency += frequencyChange;
            if (seenFrequencies.contains(frequency)) {
                println(frequency)
                return
            }
            seenFrequencies.add(frequency)
        }
    }
}