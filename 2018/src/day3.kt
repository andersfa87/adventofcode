

fun main(args: Array<String>) {
    val testData = "#1 @ 1,3: 4x4\n" +
            "#2 @ 3,1: 4x4\n" +
            "#3 @ 5,5: 2x2"
    val data = readFile("day3input.txt")

    val lines = data.split("\n")
    val regex = Regex("#([0-9]+) @ ([0-9]+,[0-9]+): ([0-9]+x[0-9]+)")

    val claims = lines.map {
        val res = regex.find(it)

        val id = res!!.groups[1]!!.value.toInt()
        val xy = res!!.groups[2]!!.value.split(",")
        val x = xy[0].toInt()
        val y = xy[1].toInt()

        val wh = res!!.groups[3]!!.value.split("x")
        val w = wh[0].toInt()
        val h = wh[1].toInt()

        Claim(id, x, y, w, h)
    }

    val map = HashMap<String, ArrayList<Claim>>()

    for (claim in claims){
        for(x in claim.x until claim.x + claim.w){
            for(y in claim.y until claim.y + claim.h) {
                val key = "$x,$y"
                val ids = map.getOrPut(key) { ArrayList() }
                ids.add(claim)
                if(ids.size >= 2) {
                    for(c in ids) {
                        c.overlapped = true
                    }
                }
            }
        }
    }

    println("Part1: ${map.filter { it.value.size > 1 }.count()}")
    println("Part2: ${claims.single { !it.overlapped }.id}")

}

class Claim(val id: Int, val x: Int, val y: Int, val w: Int, val h: Int) {
    var overlapped = false
}