import kotlin.math.floor

fun main(args: Array<String>) {
    val input = readFile("day1.txt").split("\n").map { it.toInt() };

    val part1Sum = input.sumBy { floor(it.toDouble() / 3).toInt() - 2 }

    println(part1Sum)

    val part2Sum = input.sumBy {
        calcFuelRequirement(it)
    }

    println(part2Sum)
}

fun calcFuelRequirement(mass: Int) : Int {
    var sum = 0;
    var remaining = mass
    while (remaining > 0) {
        val fuelUsage = floor(remaining.toDouble() / 3).toInt() -2
        if(fuelUsage > 0) {
            sum += fuelUsage
        }
        remaining = fuelUsage
    }
    return sum
}