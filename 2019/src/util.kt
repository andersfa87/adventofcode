import java.io.File

fun readFile(fileName: String) : String {
    return File(fileName).inputStream().use {
        val inputString = it.bufferedReader().use { it.readText() }
        return inputString.replace("\r", "")
    }
}