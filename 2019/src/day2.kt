import java.lang.Exception
import kotlin.test.assertEquals

const val OPCODE_ADD = 1
const val OPCODE_MULTIPLY = 2
const val OPCODE_HALT = 99

fun main(args: Array<String>) {
    val input = readFile("day2.txt").split(",").map { it.toInt() }.toIntArray();

//    assertEquals(3500, part1(intArrayOf(1,9,10,3,2,3,11,0,99,30,40,50)))
//    assertEquals(2, part1(intArrayOf(1,0,0,0,99)))
//    assertEquals(30, part1(intArrayOf(1,1,1,4,99,5,6,0,99)))


    println("${part1(input)}")

    println("${part2(input)}")
}

fun part1(input: IntArray) : Int {

    val instructions = input.clone()
    //1202 program alarm
    instructions[1] = 12
    instructions[2] = 2

    val program = Program(instructions)

    program.run()

    return program.instructions[0]
}

fun part2(input: IntArray) : Int {
    val targetValue = 19690720

    for(noun in 1..100) {
        for(verb in 1..100) {
            var instructions = input.clone()
            instructions[1] = noun
            instructions[2] = verb

            var program = Program(instructions)
            program.run()
            if (program.output() == targetValue) {
                return 100 * noun + verb;
            }
        }
    }
    throw Exception("not found")
}

class Program(val instructions: IntArray) {
    var programCounter = 0

    fun run() {
        while(instructions[programCounter] != OPCODE_HALT) {
            when(instructions[programCounter++]) {
                OPCODE_ADD -> {
                    add(instructions[programCounter++], instructions[programCounter++], instructions[programCounter++])
                }
                OPCODE_MULTIPLY -> {
                    multiply(instructions[programCounter++], instructions[programCounter++], instructions[programCounter++])
                }
                else -> {
                    throw Exception("DID NOT EXPECT ${instructions[programCounter]}")
                }
            }
        }
    }

    fun output() : Int {
        return instructions[0]
    }

    private fun add(index1: Int, index2: Int, outputIndex: Int) {
        instructions[outputIndex] = instructions[index1] + instructions[index2]
    }

    private fun multiply(index1: Int, index2: Int, outputIndex: Int) {
        instructions[outputIndex] = instructions[index1] * instructions[index2]
    }
}