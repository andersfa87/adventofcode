package day11

import readFile
import java.time.Duration


fun main(args: Array<String>) {


    steps("ne,ne,ne")
    steps("ne,ne,sw,sw")
    steps("ne,ne,s,s")
    steps("se,sw,se,sw,sw")

    val input = readFile("day11input.txt")

    val start = System.currentTimeMillis()
    val steps = steps(input)
    val end = System.currentTimeMillis()
    println("Steps: $steps. Took ${Duration.ofMillis(end-start)}")

}

fun steps(input:String) : Pair<Int, Int> {
    var x = 0
    var y = 0
    var z = 0
    var maxStepsSeen = 0

    for(instruction in input.split(",")) {
        when(instruction) {
            "nw" -> {
                x--
                y++
            }
            "n" -> {
                z--
                y++
            }
            "ne" -> {
                x++
                z--
            }
            "sw" -> {
                x--
                z++
            }
            "s" -> {
                y--
                z++
            }
            "se" -> {
                y--
                x++
            }
        }
        val currentSteps = (Math.abs(x - 0) + Math.abs(y - 0) + Math.abs(z - 0)) / 2
        if(currentSteps > maxStepsSeen) {
            maxStepsSeen = currentSteps
        }
    }

    return Pair((Math.abs(x - 0) + Math.abs(y - 0) + Math.abs(z - 0)) / 2, maxStepsSeen)
}