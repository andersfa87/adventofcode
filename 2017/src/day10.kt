package day10

import readFile

val testInput = "3, 4, 1, 5"

fun main(args: Array<String>) {

    val data = (0..4).toList().toIntArray()
    Part1().calculate(data, testInput)

    val input = readFile("day10input.txt")
    val data2 = (0..255).toList().toIntArray()

    Part1().calculate(data2, input)

    Hasher().calculate("1,2,3")
    Hasher().calculate("AoC 2017")
    Hasher().calculate(input)
}

class Hasher {
    fun calculate(input: String) : String {

        val data = (0..255).toList().toIntArray()
        val lengths = ArrayList<Int>()
        lengths.addAll(input.map { it.toInt() })
        lengths.addAll(intArrayOf(17,31,73,47,23).toList())


        var index = 0
        var skip = 0
        for(i in 1..64) {
            for(length in lengths) {

                val range = selectRange(data, index, length)

                replaceRange(data, index, range.reversedArray())

                val indexIncrement = length + skip
                index = (index + (indexIncrement))%data.size

                skip++

            }
        }

        //reduce
        var dense = ArrayList<Int>()

        for(i in 0..15) {
            val startIndex = i * 16
            var value = data[startIndex]

            for (j in startIndex+1 until startIndex+16) {
                value = value.xor(data[j])
            }
            dense.add(value)
        }

        return dense.map { String.format("%02X", it) }.joinToString("")
    }

    fun replaceRange(data: IntArray, startIndex: Int, range: IntArray) {
        var index = startIndex

        for (i in range.indices) {
            if(index >= data.size) {
                index = 0
            }

            data[index] = range[i]

            index++
        }
    }

    fun selectRange(data: IntArray, startIndex: Int, length: Int) : IntArray {
        val range = ArrayList<Int>()
        var index = startIndex

        for (i in 0..length-1) {
            if(index >= data.size) {
                index = 0
            }

            range.add(data[index])

            index++
        }

        return range.toIntArray()
    }
}

class Part1 {
    fun calculate(data: IntArray, input: String) {

        val lengths = input.split(",").map { it.trim().toInt() }.toIntArray()

        var index = 0
        var skip = 0
        for(length in lengths) {

            val range = selectRange(data, index, length)

            replaceRange(data, index, range.reversedArray())

            val indexIncrement = length + skip
            index = (index + (indexIncrement))%data.size

            skip++

        }
    }

    fun replaceRange(data: IntArray, startIndex: Int, range: IntArray) {
        var index = startIndex

        for (i in range.indices) {
            if(index >= data.size) {
                index = 0
            }

            data[index] = range[i]

            index++
        }
    }

    fun selectRange(data: IntArray, startIndex: Int, length: Int) : IntArray {
        val range = ArrayList<Int>()
        var index = startIndex

        for (i in 0..length-1) {
            if(index >= data.size) {
                index = 0
            }

            range.add(data[index])

            index++
        }

        return range.toIntArray()
    }
}

