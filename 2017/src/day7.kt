

class Program(val name: String, val weight: Int) {
    val subPrograms: ArrayList<Program> = ArrayList<Program>()
    val subProgramNames = ArrayList<String>()
    var parentProgram : Program? = null

    fun getCalculatedWeight() : Int {
        var sum = this.weight
        for (program in subPrograms) {
            sum += program.getCalculatedWeight()
        }
        return sum
    }
}

var day7TestInput =
        """
            pbga (66)
xhth (57)
ebii (61)
havc (66)
ktlj (57)
fwft (72) -> ktlj, cntj, xhth
qoyq (66)
padx (45) -> pbga, havc, qoyq
tknk (41) -> ugml, padx, fwft
jptl (61)
ugml (68) -> gyxo, ebii, jptl
gyxo (61)
cntj (57)""".trim()

fun main(args: Array<String>) {

    findBottomProgram(day7TestInput)

    val input = readFile("day7input.txt")

    findBottomProgram(input)
}

fun findBottomProgram(input: String) {
    val programs = HashMap<String, Program>()
    val lines = input.replace("\r", "").split("\n")

    for(line in lines) {

        val parts = line.split("->")
        val nameAndWeight = parts[0].split(" ")
        val program = Program(nameAndWeight[0], nameAndWeight[1].trim('(', ')').toInt())

        programs.put(program.name, program)

        if(parts.size > 1) {
            val subProgramNames = parts[1].trim().split(",").map { it.trim() }
            program.subProgramNames.addAll(subProgramNames)

        }

    }

    for(program in programs.values) {
        for (subProgramName in program.subProgramNames) {

            val subProgram = programs.get(subProgramName) ?: throw Exception("sub program not found")
            program.subPrograms.add(subProgram)
            subProgram.parentProgram = program
        }
    }

    val bottomProgram = programs.values.filter { it.parentProgram == null }.first()
    if(bottomProgram != null) {
        println("BottomProgram is: ${bottomProgram.name}")

        traverse(bottomProgram)

    }

}

fun traverse(program: Program) {
    val theOddOne = program.subPrograms.groupBy { it.getCalculatedWeight() }.filter { it.value.size == 1 }.flatMap { it.value }.singleOrNull()

    if(theOddOne == null) {
        val parent = program.parentProgram
        if(parent != null) {
            println("${parent.name} = ${parent.weight}")
        }

    }
    else {
        if(theOddOne.subPrograms.any()) {
            for (subProgram in theOddOne.subPrograms) {
                traverse(subProgram)
            }
        } else {
            println("${theOddOne.name} is skæv")
        }
    }
}