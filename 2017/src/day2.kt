import java.io.File
import java.io.InputStream

val testInput =
"""5 1 9 5
7 5 3
2 4 6 8"""



fun main(args: Array<String>) {
    checksum(testInput)

    part1()
    part2()
}

fun checksum(input: String) {
    var lines = input
            .split("\n")
            .filter { !it.trim().isEmpty() }
            .map {
                it.trim().split(" ", "\t")
                        .map { it.toInt() }.toIntArray()
            }

    var sum = 0
    for (line in lines){

        val min = line.min()
        val max = line.max()

        if(min != null && max != null){
            sum += max-min
        }
    }

    println(sum)
    println()
}

fun part1() {
    val inputStream: InputStream = File("day2part1.txt").inputStream()

    val inputString = inputStream.bufferedReader().use { it.readText() }

    inputStream.close()

    checksum(inputString)
}

fun part2() {
    val inputStream = File("day2part1.txt").inputStream()

    val inputString = inputStream.bufferedReader().use { it.readText() }

    inputStream.close()

    var lines = inputString
            .split("\n")
            .filter { !it.trim().isEmpty() }
            .map {
                it.trim().split(" ", "\t")
                        .map { it.toInt() }.toIntArray()
            }

    var sum = 0
    for (line in lines){

        line.forEachIndexed { index, number ->

            line.forEachIndexed { innerIndex, innerNumber ->
                if(innerIndex != index) {
                    if(number % innerNumber == 0){
                        sum += number / innerNumber
                    }
                }
            }
        }

    }

    println(sum)
    println()
}