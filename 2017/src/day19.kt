package day19

import readFile

val testInput =
"""     |
     |  +--+
     A  |  C
 F---|----E|--+
     |  |  |  D
     +B-+  +--+"""


fun main(args: Array<String>) {

    val input = readFile("day19input.txt")

    val grid = input.split("\n").map { it.toCharArray() }

    var x = grid[0].indexOf('|')
    var y = 0

    val letters = StringBuilder()
    var direction = Direction.Down
    var steps = 0

    loop@ while(true) {

        when(direction) {

            Direction.Down -> {
                y++
                steps++
                val char = grid[y][x]
                if(char == ' ') {
                    break@loop
                }
                else if(char == '+'){
                    direction = if (x-1 > 0 && grid[y][x-1] == '-' || grid[y][x-1].isLetter()) Direction.Left else Direction.Right
                }
                else if (char.isLetter()) {
                    letters.append(char)
                }
            }
            Direction.Left -> {
                x--
                steps++
                val char = grid[y][x]
                if(char == ' ') {
                    break@loop
                }
                else if(char == '+'){
                    direction = if(y-1 < grid.size && x < grid[y-1].size && (grid[y-1][x] == '|' || grid[y-1][x].isLetter())) Direction.Up else Direction.Down
                }
                else if (char.isLetter()) {
                    letters.append(char)
                }
            }
            Direction.Right -> {
                x++
                steps++
                val char = grid[y][x]
                if(char == ' ') {
                    break@loop
                }
                else if(char == '+'){
                    direction = if(y-1 < grid.size && x < grid[y-1].size && (grid[y-1][x] == '|' || grid[y-1][x].isLetter())) Direction.Up else Direction.Down
                }
                else if (char.isLetter()) {
                    letters.append(char)
                }
            }
            Direction.Up -> {
                y--
                steps++
                val char = grid[y][x]
                if(char == ' ') {
                    break@loop
                }
                else if(char == '+'){
                    direction = if (x-1 > 0 && grid[y][x-1] == '-' || grid[y][x-1].isLetter()) Direction.Left else Direction.Right
                }
                else if (char.isLetter()) {
                    letters.append(char)
                }
            }
        }
    }

    println(letters)
    println("steps: $steps")

}

fun pushLetter(letters: StringBuilder, char: Char): Boolean {
    if(char == '|' || char == '-' || char == '+') {
        return false
    }
    letters.append(char)
    return true
}

enum class Direction {
    Up,
    Down,
    Left,
    Right
}
