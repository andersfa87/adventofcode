package day12

import readFile

val testInput = """
    0 <-> 2
1 <-> 1
2 <-> 0, 3, 4
3 <-> 2, 4
4 <-> 2, 3, 6
5 <-> 6
6 <-> 4, 5
    """.trim()

fun main(args: Array<String>) {

    val testPrograms = parse(testInput)
    val testCount = getCount(testPrograms["0"] ?: throw Exception("program 0 not found"))
    println("$testCount programs in group 0")

    val input = readFile("day12input.txt")
    val programs = parse(input)
    val count = getCount(programs["0"] ?: throw Exception("program 0 not found"))
    println("$count programs in group 0")

    val groups = getGroups(programs)
    println("groupcount = ${groups.values.groupBy { it }.size}")

}

fun getGroups(programs: HashMap<String, Program>) : HashMap<Program, Group> {
    val programGroups = HashMap<Program, Group>()

    for (program in programs.values) {
        if(!programGroups.containsKey(program)) {
            val group = Group()
            programGroups.put(program, group)
            walk(group, program, programGroups)
        }
    }
    return programGroups
}

fun walk(group: Group, program: Program, programGroups: HashMap<Program, Group>) {
    for (program in program.connections.values) {
        if(!programGroups.containsKey(program)) {
            programGroups.put(program, group)
            walk(group, program, programGroups)
        }
    }
}

fun getCount(program: Program) : Int {
    val seen = HashSet<Program>()

    for (p in program.connections.values) {
        seen.add(p)
        walkCount(p, seen)
    }

    return seen.size
}

fun walkCount(program: Program, seen: HashSet<Program>) {
    for (p in program.connections.values) {
        if(seen.add(p)) {
            walkCount(p, seen)
        }
    }
}

fun parse(input: String) : HashMap<String, Program> {
    val lines = input.replace("\r", "").split("\n")

    val programs = HashMap<String, Program>()

    for (line in lines) {
        val id = line.substring(0, line.indexOf('<')).trim()

        val program = programs.getOrPut(id, { Program(id) })

        val connections = line.substring(line.indexOf('>')+1).trim().split(",").map { it.trim() }

        for(connection in connections) {
            val connectedProgram = programs.getOrPut(connection, { Program(connection) })
            program.connections.putIfAbsent(connectedProgram.id, connectedProgram)
            connectedProgram.connections.putIfAbsent(program.id, program)
        }
    }

    return programs
}

class Group() {
    val programs = HashSet<Program>()
}

class Program(val id: String) {
    val connections = HashMap<String, Program>()
}