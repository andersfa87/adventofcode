package day16

import readFile
import java.time.Duration

val testInput = """
    s1
    x3/4
    pe/b
    """.trim()

fun main(args: Array<String>) {


    //doit('a', 'e', testInput.split("\n").map { it.trim() })


    val input = readFile("day16input.txt").split(",")

    val start = System.currentTimeMillis()
    doit('a', 'p', input)
    val end = System.currentTimeMillis()
    println("Duration: ${Duration.ofMillis(end-start)}")



}

fun doit(startLetter: Char, endLetter: Char, instructions: List<String>) {
    var programs = (startLetter.toInt()..endLetter.toInt())
            .map { it.toChar() }
            .toCharArray()

    val numberOfDances = 1000000000
    var iterations = 0L
    val danceResults = ArrayList<String>(numberOfDances)

    while(iterations < numberOfDances) {
        for (instruction in instructions) {
            val type = instruction[0]
            when (type) {
                's' -> {
                    val count = instruction.substring(1).toInt()

                    val startRange = selectRange(programs, 0, programs.size - count)
                    val endRange = selectRange(programs, programs.size - count, count);

                    programs = endRange.plus(startRange)
                }
                'x' -> {
                    val positions = instruction.substring(1).split("/").map { it.toInt() }
                    val temp = programs[positions[0]]
                    programs[positions[0]] = programs[positions[1]]
                    programs[positions[1]] = temp
                }
                'p' -> {
                    val positions = instruction.substring(1).split("/").map { it[0] }.map { programs.indexOf(it) }
                    val temp = programs[positions[0]]
                    programs[positions[0]] = programs[positions[1]]
                    programs[positions[1]] = temp
                }
            }
        }

        val result = String(programs)

        val existingIndex = danceResults.indexOf(String(programs))
        if(existingIndex >= 0) {
            println("After ${danceResults.size} iterations, we found the same pattern at index: $existingIndex")

            var finalIndex = ((numberOfDances - danceResults.size - 1)) % danceResults.size
            println("Final index should be: $finalIndex, which is ${danceResults[finalIndex]}")
            break
        }
        else {
            danceResults.add(result)
        }
        iterations++
    }

}



fun selectRange(data: CharArray, startIndex: Int, length: Int) : CharArray {
    val range = ArrayList<Char>()
    var index = startIndex

    for (i in 0..length-1) {
        if(index >= data.size) {
            index = 0
        }

        range.add(data[index])

        index++
    }

    return range.toCharArray()
}