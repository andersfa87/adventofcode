package day21

import day13.input
import readFile

val startingState =
""".#.
..#
###"""

val testInput =
"""../.# => ##./#../...
.#./..#/### => #..#/..../..../#..#"""

fun main(args: Array<String>) {

    val input = readFile("day21input.txt")

    val rules = input
            .split("\n")
            .map {
                val inputOutput = it.split("=>").map { it.trim() }
                val input = inputOutput[0]
                val output = inputOutput[1]
                Rule(input.split("/").map { it.toCharArray() }, output.split("/").map { it.toCharArray() })
            }

    var grid = startingState.split("\n").map { it.toCharArray() }

    printBlock(grid)
    for(i in 1..18) {

        var squares = getSquares(grid)
        squares = transformSquares(squares, rules)
        grid = mergeSquares(squares)

        //println("=============== ITERATION: ${i}")
        //printBlock(grid)
    }

    val turnedOn = grid.sumBy { it.count { it == '#' } }

    println("${turnedOn} turned on")
    //println(blocks)
}

fun transformSquares(squares: List<List<CharArray>>, rules: List<Rule>): List<List<CharArray>> {
    return squares.map {
        val square = it

        val rule = rules.find {
//            blocksMatch(square, it.inputBlock)
//                    ||
//                    blocksMatch(square, rotate90(it.inputBlock))
//                    ||
//                    blocksMatch(square, rotate180(it.inputBlock))
//                    ||
//                    blocksMatch(square, rotate270(it.inputBlock))
//                    ||
//                    blocksMatch(square, flipVertically(it.inputBlock))
//                    ||
//                    blocksMatch(square, flipHorizontally(it.inputBlock))
//                    ||
//                    blocksMatch(square, rotate90(flipHorizontally(it.inputBlock)))
            it.matches(square)
        }

        (rule ?: throw Exception("rule not found:\n${square.map { it.contentToString() }.joinToString("\n")}")).outputBlock
    }
}

fun getSquares(grid: List<CharArray>): List<List<CharArray>> {
    val squareSize = if (grid.size % 2 == 0) 2 else 3
    val num = grid.size / squareSize
    val squares = ArrayList<List<CharArray>>(num)

    for(squareRow in 0 until num) {

        for(squareColumn in 0 until num) {

            val startY = squareRow * squareSize
            val startX = squareColumn * squareSize
            val square = ArrayList<CharArray>((0 until squareSize).map { CharArray(squareSize) })

            for(y in startY until startY + squareSize) {
                for(x in startX until startX + squareSize) {
                    square[y-startY][x-startX] = grid[y][x]
                }
            }
            squares.add(square)

        }

    }
    return squares
}

fun mergeSquares(squares: List<List<CharArray>>) : List<CharArray> {

    if(squares.size == 1) return squares[0]

    val squareSize = squares[0].size
    val rowCount = Math.sqrt(squares.size.toDouble()).toInt()
    //val rowCount = squares[0].size * (Math.floor(Math.sqrt(squares.size.toDouble()))).toInt()

    val grid = ArrayList<CharArray>((0 until rowCount * squares[0].size).map { CharArray(rowCount * squares[0].size) })

    for(squareIndex in squares.indices) {
        val square = squares[squareIndex]

        for(y in 0 until square.size) {
            for(x in 0 until square[y].size) {

                //val y1 = (squareIndex * square.size) % squareSize + y

                val y1 = (Math.floor(squareIndex.toDouble() / Math.sqrt(squares.size.toDouble())) * squareSize + y).toInt()

                val x1 = ((squareIndex * squareSize + x)) % (squareSize * rowCount)

                grid[y1][x1] = square[y][x]
            }
        }
    }

    return grid
}


class Rule(val inputBlock: List<CharArray>, val outputBlock: List<CharArray>) {
    val sets = HashSet<String>()

    fun map(grid: List<CharArray>) : String {
        return grid.map { it.joinToString(",") }.joinToString(",")
    }

    fun matches(grid: List<CharArray>) : Boolean {
        return sets.contains(map(grid))
    }

    init {

        sets.add(map(inputBlock))
        sets.add(map(rotate90(inputBlock)))
        sets.add(map(rotate180(inputBlock)))
        sets.add(map(rotate270(inputBlock)))

        sets.add(map(flipHorizontally(inputBlock)))
        sets.add(map(rotate90(flipHorizontally(inputBlock))))
        sets.add(map(rotate180(flipHorizontally(inputBlock))))
        sets.add(map(rotate270(flipHorizontally(inputBlock))))
    }
}

fun expand(block: List<CharArray>, rule: Rule) : List<List<CharArray>> {
    val blocks = ArrayList<List<CharArray>>()

    for(i in 0 until block.size * block.size) {
        blocks.add(rule.outputBlock)
    }

    return blocks
}

fun rotate90(block: List<CharArray>) : List<CharArray> {

    val rotated = block.map { CharArray(it.size) }

    for (y in 0 until block.size) {
        for (x in 0 until block[0].size) {
            rotated[y][x] = block[block[0].size - x - 1][y];
        }
    }

    return rotated
}

fun rotate180(block: List<CharArray>) : List<CharArray> {
    return rotate90(rotate90(block))
}

fun rotate270(block: List<CharArray>) : List<CharArray> {
    return rotate180(rotate90(block))
}

fun flipHorizontally(block: List<CharArray>) : List<CharArray> {

    val flipped = ArrayList<CharArray>(block.size)

    (0 until block.size).mapTo(flipped) { block[it].reversedArray() }

    return flipped
}

fun flipVertically(block: List<CharArray>) : List<CharArray> {
    return rotate90(rotate90(flipHorizontally(block)))
}

fun blocksMatch(block: List<CharArray>, otherBlock: List<CharArray>) : Boolean {
    if(block.size != otherBlock.size) return false
    return (0 until block.size).all { block[it].contentToString() == otherBlock[it].contentToString() }
}

fun printBlock(block: List<CharArray>) {
    for (line in block) {
        println("${line.contentToString()}")
    }
}