package day15

import java.math.BigInteger
import java.time.Duration

fun main(args: Array<String>) {
    //Generator A starts with 591
    //Generator B starts with 393

    //asd(65, 8921)
    val start = System.currentTimeMillis()
    asd2(591, 393)
    val end = System.currentTimeMillis()
    println("Duration: ${Duration.ofMillis(end-start)}")
}

fun asd(inputA: Long, inputB: Long) {
    val iterations = 40000000
    val factorA = 16807L
    val factorB = 48271L
    val divideBy = 2147483647L

    val a = LongArray(iterations)
    val b = LongArray(iterations)

    var total = 0

    for(i in 0 until  iterations) {
        val previousA = if (i == 0) inputA else a[i-1]
        val previousB = if (i == 0) inputB else b[i-1]

        a[i] = (previousA * factorA) % divideBy
        b[i] = (previousB * factorB) % divideBy

        var binaryA = a[i].toString(2)
        var binaryB = b[i].toString(2)

        if(binaryA.length < 16) {
            binaryA = (0..(16-binaryA.length)).map { "0" }.joinToString("").plus(binaryA)
        }

        if(binaryB.length < 16) {
            binaryB = (0..(16-binaryB.length)).map { "0" }.joinToString("").plus(binaryB)
        }

        if(binaryA.reversed().take(16).equals(binaryB.reversed().take(16))) {
            total ++
        }
    }

    println("Total: $total")

}

fun asd2(inputA: Long, inputB: Long) {

    val pairs = 5000000
    val factorA = 16807L
    val factorB = 48271L
    val divideBy = 2147483647L

    val a = ArrayList<Long>()
    val b = ArrayList<Long>()

    var total = 0

    var previousA = inputA
    while (a.size <= pairs) {
        val number = (previousA * factorA) % divideBy

        if(number % 4 == 0L) {
            a.add(number)
        }
        previousA = number
    }

    var previousB = inputB
    while (b.size <= pairs) {
        val number = (previousB * factorB) % divideBy

        if(number % 8 == 0L) {
            b.add(number)
        }
        previousB = number
    }

    for(i in a.indices) {
        var binaryA = a[i].toString(2)
        var binaryB = b[i].toString(2)

        if(binaryA.length < 16) {
            binaryA = (0..(16-binaryA.length)).map { "0" }.joinToString("").plus(binaryA)
        }

        if(binaryB.length < 16) {
            binaryB = (0..(16-binaryB.length)).map { "0" }.joinToString("").plus(binaryB)
        }

        if(binaryA.reversed().take(16).equals(binaryB.reversed().take(16))) {
            total ++
        }
    }

    println("Total: $total")

}