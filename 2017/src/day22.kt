package day22

import readFile


val testInput =
        """..#
#..
..."""

fun main(args: Array<String>) {

    val input = readFile("day22input.txt")

    val grid = HashMap<String, State>()

    val initialState = input.split("\n").map { it.toCharArray() }

    for(y in initialState.indices) {

        for(x in initialState[y].indices) {

            grid.set("$x,$y", if(initialState[y][x] == '.') State.Clean else State.Infected)

        }

    }

    var x = initialState.size / 2
    var y = initialState[0].size / 2
    var direction = Direction.Up

    var infectionsCaused = 0
    for(i in 0 until 10000000){

        var state = grid["$x,$y"] ?: State.Clean
        //turn
        //infect/clean
        when(state) {
            State.Clean -> {
                direction = turn(direction, Turn.Left)
                state = State.Weakened
            }
            State.Weakened -> {
                state = State.Infected
                infectionsCaused++
            }
            State.Infected -> {
                direction = turn(direction, Turn.Right)
                state = State.Flagged
            }
            State.Flagged -> {
                direction = turn(direction, Turn.Back)
                state = State.Clean
            }
        }

        grid["$x,$y"] = state

        //move
        when(direction) {
            Direction.Up -> y--
            Direction.Down -> y++
            Direction.Left -> x--
            Direction.Right -> x++
        }

    }

    println("$infectionsCaused")

}

fun turn(currentDirection: Direction, turn: Turn): Direction {
    //# -> Turn right
    //. -> Turn left
    when(currentDirection) {
        Direction.Up -> {
            when(turn) {
                Turn.Left -> return Direction.Left
                Turn.Right -> return Direction.Right
                else -> return Direction.Down
            }
        }
        Direction.Down -> {
            when(turn) {
                Turn.Left -> return Direction.Right
                Turn.Right -> return Direction.Left
                else -> return Direction.Up
            }
        }
        Direction.Left -> {
            when(turn) {
                Turn.Left -> return Direction.Down
                Turn.Right -> return Direction.Up
                else -> return Direction.Right
            }
        }
        Direction.Right -> {
            when(turn) {
                Turn.Left -> return Direction.Up
                Turn.Right -> return Direction.Down
                else -> return Direction.Left
            }
        }
    }
}

enum class Direction {
    Up,
    Down,
    Left,
    Right
}

enum class State {
    Clean,
    Weakened,
    Infected,
    Flagged
}

enum class Turn {
    Left,
    Right,
    Back
}