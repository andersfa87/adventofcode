package day9

import org.junit.jupiter.api.Test
import readFile
import kotlin.test.assertEquals


class TestClass {
    @Test
    fun test1(){
        assertEquals(1, Parser("{}").getScore().first)
    }
    @Test
    fun test2(){
        assertEquals(6, Parser("{{{}}}").getScore().first)
    }
    @Test
    fun test3(){
        assertEquals(5, Parser("{{},{}}").getScore().first)
    }
    @Test
    fun test4(){
        assertEquals(16, Parser("{{{},{},{{}}}}").getScore().first)
    }
    @Test
    fun test5(){
        assertEquals(1, Parser("{<a>,<a>,<a>,<a>}").getScore().first)
    }
    @Test
    fun test6(){
        assertEquals(9, Parser("{{<ab>},{<ab>},{<ab>},{<ab>}}").getScore().first)
    }
    @Test
    fun test7(){
        assertEquals(9, Parser("{{<!!>},{<!!>},{<!!>},{<!!>}}").getScore().first)
    }
    @Test
    fun test8(){
        assertEquals(3, Parser("{{<a!>},{<a!>},{<a!>},{<ab>}}").getScore().first)
    }

    @Test fun test9() {
        assertEquals(0, Parser("<>").getScore().second)
    }
    @Test fun test10() {
        assertEquals(17, Parser("<random characters>").getScore().second)
    }
    @Test fun test11() {
        assertEquals(3, Parser("<<<<>").getScore().second)
    }
    @Test fun test12() {
        assertEquals(2, Parser("<{!>}>").getScore().second)
    }
    @Test fun test13() {
        assertEquals(0, Parser("<!!>").getScore().second)
    }
    @Test fun test14() {
        assertEquals(0, Parser("<!!!>>").getScore().second)
    }
    @Test fun test15() {
        assertEquals(10, Parser("<{o\"i!a,<{i<a>").getScore().second)
    }
}