package day3

import day13.input

fun main(args: Array<String>) {
    println("distance: ${calculateSteps(1)}")
    println("distance: ${calculateSteps(12)}")
    println("distance: ${calculateSteps(23)}")
    println("distance: ${calculateSteps(1024)}")

    println("distance: ${calculateSteps(368078)}")


    println("part2: ${findFirstValue(368078)}")
}


fun calculateSteps(squareNumber: Int) : Int {
    var x = 0
    var y = 0
    var directionChanges = 0
    var stepsToTakeInDirection = 1
    var stepsTakenInDirection = 0
    var direction = Direction.Right

    for (i in 1 until squareNumber) {

        stepsTakenInDirection++

        when(direction) {
            Direction.Right -> {
                x++
                if(stepsTakenInDirection == stepsToTakeInDirection) {
                    direction = Direction.Up
                    directionChanges++
                    stepsTakenInDirection = 0
                }
            }
            Direction.Up -> {
                y++
                if(stepsTakenInDirection == stepsToTakeInDirection) {
                    direction = Direction.Left
                    directionChanges++
                    stepsTakenInDirection = 0
                }
            }
            Direction.Left -> {
                x--
                if(stepsTakenInDirection == stepsToTakeInDirection) {
                    direction = Direction.Down
                    directionChanges++
                    stepsTakenInDirection = 0
                }
            }
            Direction.Down -> {
                y--
                if(stepsTakenInDirection == stepsToTakeInDirection) {
                    direction = Direction.Right
                    directionChanges++
                    stepsTakenInDirection = 0
                }
            }
        }

        if(stepsTakenInDirection == 0 && directionChanges % 2 == 0) {
            stepsToTakeInDirection ++
        }

    }

    val distance = (Math.abs(x - 0) + Math.abs(y - 0))

    return distance
}

fun findFirstValue(squareNumber: Int) : Int {
    var x = 0
    var y = 0
    var directionChanges = 0
    var stepsToTakeInDirection = 1
    var stepsTakenInDirection = 0
    var direction = Direction.Right
    val data = HashMap<String, Int>()
    data["0,0"] = 1

    while(true) {

        stepsTakenInDirection++

        when(direction) {
            Direction.Right -> {
                x++
                if(stepsTakenInDirection == stepsToTakeInDirection) {
                    direction = Direction.Up
                    directionChanges++
                    stepsTakenInDirection = 0
                }
            }
            Direction.Up -> {
                y++
                if(stepsTakenInDirection == stepsToTakeInDirection) {
                    direction = Direction.Left
                    directionChanges++
                    stepsTakenInDirection = 0
                }
            }
            Direction.Left -> {
                x--
                if(stepsTakenInDirection == stepsToTakeInDirection) {
                    direction = Direction.Down
                    directionChanges++
                    stepsTakenInDirection = 0
                }
            }
            Direction.Down -> {
                y--
                if(stepsTakenInDirection == stepsToTakeInDirection) {
                    direction = Direction.Right
                    directionChanges++
                    stepsTakenInDirection = 0
                }
            }
        }

        var sum = 0

        sum += data["${x-1},${y-1}"] ?: 0
        sum += data["${x},${y-1}"] ?: 0
        sum += data["${x+1},${y-1}"] ?: 0

        sum += data["${x-1},${y}"] ?: 0
        sum += data["${x-1},${y+1}"] ?: 0
        sum += data["${x},${y+1}"] ?: 0
        sum += data["${x+1},${y+1}"] ?: 0
        sum += data["${x+1},${y}"] ?: 0


        data["$x,$y"] = sum

        if(stepsTakenInDirection == 0 && directionChanges % 2 == 0) {
            stepsToTakeInDirection ++
        }

        if(sum > squareNumber) {
            return sum
        }

    }
    throw Exception("Did not find value larger than input")
}

enum class Direction {
    Up,
    Down,
    Left,
    Right
}