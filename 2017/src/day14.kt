package day14

import day10.Hasher
import java.math.BigInteger
import java.time.Duration

fun main(args: Array<String>) {

    val testInput = "flqrgnkx"
    val testSlots = getUsedSlots(testInput)
    println("Test = ${testSlots.first}")

    val input = "ljoxqyyw"
    var start = System.currentTimeMillis()
    val slots = getUsedSlots(input)
    var end = System.currentTimeMillis()
    println("Part1 = ${slots.first}")
    println("Duration: ${Duration.ofMillis(end-start)}")

    start = System.currentTimeMillis()
    val groups = HashMap<String, Group>()
    val disk = slots.second

    for(y in disk.indices) {

        val row = disk[y]

        for(x in row.indices) {

            val slotUsed = row[x]

            if(slotUsed) {

                var groupFoundLeft: Group? = null
                if (x > 0) {
                    groupFoundLeft = groups.get("${x - 1},$y")
                }

                var groupFoundUp: Group? = null
                if (y > 0) {
                    groupFoundUp = groups.get("$x,${y - 1}")
                }

                if (groupFoundLeft != null && groupFoundUp != null && groupFoundLeft != groupFoundUp) {
                    groups["$x,$y"] = mergeGroups(groupFoundLeft, groupFoundUp, groups)
                } else {
                    if (groupFoundLeft != null) {
                        groups["$x,$y"] = groupFoundLeft
                    } else if (groupFoundUp != null) {
                        groups["$x,$y"] = groupFoundUp
                    } else {
                        groups["$x,$y"] = Group()
                    }
                }
            }
        }
    }

    val groupCount = groups.values.groupBy { it }.count()
    end = System.currentTimeMillis()
    println("Duration: ${Duration.ofMillis(end-start)}")
    println("Part2 = $groupCount")
}

fun mergeGroups(groupFoundLeft: Group, groupFoundUp: Group, groups: HashMap<String, Group>) : Group {
    for(key in groups.entries.filter { it.value == groupFoundLeft }.map { it.key }) {
        groups[key] = groupFoundUp
    }
    return groupFoundUp
}

class Group {

}

fun getUsedSlots(input: String) : Pair<Int, Array<Array<Boolean>>> {
    val disk = Array<Array<Boolean>>(128, { Array<Boolean>(128, { false }) })
    val hasher = Hasher()

    var usedSlots = 0

    for (i in 0..127) {
        val hash = hasher.calculate("$input-$i")
        var bits = ""
        for ( hex in hash){
            val bitsPart = String.format("%04d", BigInteger(Integer.toBinaryString(Integer.parseInt(hex.toString(), 16))))
            bits += bitsPart
        }

        for(j in 0..127) {
            disk[i][j] = bits[j] == '1'
            if(disk[i][j]) usedSlots++
        }
    }

    return Pair(usedSlots, disk)
}