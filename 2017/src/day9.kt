package day9


import readFile
import java.time.Duration

fun main(args: Array<String>) {


    val input = readFile("day9input.txt")
    val start = System.currentTimeMillis()
    val result = Parser(input).getScore()
    val end = System.currentTimeMillis()
    println("Results = ${result}")
    println("Took: ${Duration.ofMillis(end-start)}")

}

class Parser(val input: String) {

    fun getScore() : Pair<Int, Int>{
        var sum = 0
        var garbageCount = 0
        var level = 0
        var skip = false
        var garbageOpen = false
        loop@ for(i in input.indices) {

            if(skip) {
                skip = false
                continue@loop
            }

            val char = input[i]
            when (char) {
                '{' -> {
                    if(garbageOpen) {
                        garbageCount++
                        continue@loop
                    }
                    level++
                }
                '}' -> {
                    if(garbageOpen) {
                        garbageCount++
                        continue@loop
                    }
                    sum+= level
                    level--
                }
                '<' -> {
                    //garbage start
                    if(garbageOpen) {
                        garbageCount++
                    }
                    else {
                        garbageOpen = true
                    }
                }
                '>' -> {
                    //garbage end
                    garbageOpen = false
                }
                '!' -> {
                    skip = true
                }
                else -> {
                    if(garbageOpen) {
                        garbageCount++
                    }
                }
            }
        }

        return Pair(sum, garbageCount)
    }

}