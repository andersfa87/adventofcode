import java.time.Duration

fun main(args: Array<String>) {

    var banks = intArrayOf(0,2,7,0)

    distributeThatShit(banks)

    var input = intArrayOf(4,1,15,12,0,9,9,5,5,8,7,3,14,5,12,3)

    val start = System.currentTimeMillis()
    distributeThatShit(input)
    val end = System.currentTimeMillis()
    println("Part1: ${Duration.ofMillis(end-start)}")
}

fun distributeThatShit(banks: IntArray) {
    var map = HashMap<Int, Int>()
    var iteration = 0
    while (!map.contains(banks.contentHashCode())) {
        map.put(banks.contentHashCode(), iteration++)

        var index = banks.indices.maxBy { banks[it] } ?: throw Exception("shit")
        var remainder = banks[index]
        banks[index] = 0
        //redistribute
        while (remainder > 0) {
            index++
            if(index >= banks.size) {
                index = 0
            }
            banks[index]++
            remainder--
        }
    }
    println("Total steps: ${map.size}")
    val firstOccurance = map.get(banks.contentHashCode())
    if(firstOccurance != null) {
        println("Steps to reoccur: " + (iteration-firstOccurance))
    }
}