package day20

import readFile

val testInput =
"""p=< 3,0,0>, v=< 2,0,0>, a=<-1,0,0>
p=< 4,0,0>, v=< 0,0,0>, a=<-2,0,0>"""


fun main(args: Array<String>) {

    val input = readFile("day20input.txt")

    val data = input.split("\n")
            .map {
                val pStart = it.indexOf('<')
                val pEnd = it.indexOf('>', pStart)

                val vStart = it.indexOf('<', pEnd)
                val vEnd = it.indexOf('>', vStart)

                val aStart = it.indexOf('<', vEnd)
                val aEnd = it.indexOf('>', aStart)

                val positionValues = it.substring(pStart+1, pEnd).split(",").map { it.trim().toInt() }
                val velocityValues = it.substring(vStart+1, vEnd).split(",").map { it.trim().toInt() }
                val accelerationValues = it.substring(aStart+1, aEnd).split(",").map { it.trim().toInt() }

                Particle(
                        Point3D(positionValues[0], positionValues[1], positionValues[2]),
                        Point3D(velocityValues[0], velocityValues[1], velocityValues[2]),
                        Point3D(accelerationValues[0], accelerationValues[1], accelerationValues[2])
                )
            }


    part2(ArrayList(data))

}

fun part2(data: ArrayList<Particle>) {
    var noneDestroyedIterations = 0
    var iterations = 0
    while (true) {
        var closestChanged = false
        for(i in data.indices) {
            val particle = data[i]
            step(particle)
        }

        //if any overlaps
        val toRemove = data.filter {
            val p = it
            data.any { it != p && it.intersects(p) }
        }
        if(toRemove.any()) {
            data.removeAll(toRemove)
            println("Collisions: ${toRemove.size} removed")
            noneDestroyedIterations = 0
        } else {
            noneDestroyedIterations++
        }

        if(noneDestroyedIterations == 1000){
            break;
        }

        iterations++
    }

    println("I think there are ${data.size} particles remaining")
}

fun part1(data: List<Particle>)  {

    var closest = data[0]
    var closestForIterations = 0
    var iterations = 0
    while (true) {
        var closestChanged = false
        for(i in data.indices) {
            val particle = data[i]
            step(particle)

            if(particle.distance() < closest.distance()) {
                closest = particle
                closestChanged = true
                println("New closest found: ${i}")
            }
        }

        if(closestChanged) {
            closestForIterations = 0
        } else {
            closestForIterations++
        }

        if(closestForIterations == 1000) {
            break;
        }
        iterations++
    }

    println("I think ${data.indexOf(closest)} is the one. Total iterations: $iterations")
}

fun step(particle: Particle) {



    //increase velocity by acceleration
    particle.updateVelocity()
    //update position by velocity
    particle.updatePosition()

}

class Particle(private val position: Point3D, val velocity: Point3D, val acceleration: Point3D) {

    var dead = false

    fun intersects(particle: Particle) : Boolean{
        return particle.position.x == this.position.x && particle.position.y == this.position.y && particle.position.z == this.position.z
    }

    fun updateVelocity() {
        this.velocity.x += acceleration.x
        this.velocity.y += acceleration.y
        this.velocity.z += acceleration.z
    }

    fun updatePosition() {
        this.position.x += this.velocity.x
        this.position.y += this.velocity.y
        this.position.z += this.velocity.z
    }

    fun distance() : Int {
        return (Math.abs(this.position.x - 0) + Math.abs(this.position.y - 0) + Math.abs(this.position.z - 0)) / 2
    }

    override fun toString(): String {
        return "<${position.x},${position.y},${position.z}>"
    }
}

class Point3D(var x: Int, var y: Int, var z: Int){}