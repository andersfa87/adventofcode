package day13

import java.time.Duration
import javax.print.attribute.standard.Severity

val testInput = """
    0: 3
1: 2
4: 4
6: 4""".trim()

val input = """0: 3
1: 2
2: 5
4: 4
6: 6
8: 4
10: 8
12: 8
14: 6
16: 8
18: 6
20: 6
22: 8
24: 12
26: 12
28: 8
30: 12
32: 12
34: 8
36: 10
38: 9
40: 12
42: 10
44: 12
46: 14
48: 14
50: 12
52: 14
56: 12
58: 12
60: 14
62: 14
64: 12
66: 14
68: 14
70: 14
74: 24
76: 14
80: 18
82: 14
84: 14
90: 14
94: 17
""".trim()

fun main(args: Array<String>) {

    var startPicoSecond = 0

    val firewall = getFirewall(input)
    val start = System.currentTimeMillis()
    while(true) {
        val caught = calcSeverity2(firewall, startPicoSecond)
        if(!caught) {
            println("Start in $startPicoSecond caused 0 severity")
            break
        }
//        if(startPicoSecond%10000 == 0) {
//            println("Current test value: $startPicoSecond")
//        }
        startPicoSecond++
    }
    val end = System.currentTimeMillis()

    println("Duration: ${Duration.ofMillis(end-start)}")

}

fun getFirewall(input: String) : HashMap<Int, Int> {
    var firewall = HashMap<Int, Int>()

    for(layer in input.split("\n")) {

        val layerData = layer.split(":")
        val depth = layerData[0].toInt()
        val range = layerData[1].trim().toInt()

        firewall.put(depth, range)
    }
    return firewall
}

fun calcSeverity2(firewall: HashMap<Int, Int>, startPicoSecond: Int) : Boolean {
    var picoSecond = startPicoSecond
    var depth = 0

    val maxFirewallDepth = firewall.keys.max() ?: throw Exception("firewall empty")
    var caught = false

    while(!caught && depth <= maxFirewallDepth) {

        val range = firewall.get(depth)
        if(range != null) {

            val scannerIndex = getScannerIndex(depth, range, picoSecond)


            if(scannerIndex == 0) {
                caught = true
                break
            }

        }

        depth++
        picoSecond++
    }

    return caught
}

fun calcSeverity(input: String, startPicoSecond: Int) : Int {
    var picoSecond = startPicoSecond
    var firewall = getFirewall(input)

    val maxFirewallDepth = firewall.keys.max() ?: throw Exception("firewall empty")
    var severity = 0

    while(picoSecond <= maxFirewallDepth) {

        val range = firewall.get(picoSecond)
        if(range != null) {

            val scannerIndex = getScannerIndex(picoSecond, range, picoSecond)


            if(scannerIndex == 0) {
                //get hit
                severity += picoSecond * range
            }

        }


        picoSecond++
    }

    return severity
}
//depth, (picosecond, index)
val cache = HashMap<Int, Cached>()

fun getScannerIndex(depth: Int, range: Int, picoSecond: Int) : Int {

    val cached = cache.get(depth)

    var index = if(cached != null) cached.index else 0
    var startPicoSecond = if(cached != null) cached.picoSecond else 0
    var increment = if(cached != null) cached.increment else true

    for(sec in startPicoSecond until picoSecond) {
        if(increment) {
            index++
            if(index == range-1) {
                increment = false
            }
        }
        else {
            index--
            if(index == 0){
                increment = true
            }
        }
    }

    cache.put(depth, Cached(index, picoSecond, increment))

    return index
}

class Cached(val index: Int, val picoSecond: Int, val increment: Boolean) {

}