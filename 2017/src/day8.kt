package day8

import readFile

val testInput =
        """b inc 5 if a > 1
a inc 1 if b < 5
c dec -10 if a >= 1
c inc -20 if c == 10""".trim()

fun main(args: Array<String>) {


    processInstructions(testInput)


    val input = readFile("day8input.txt")
    processInstructions(input)

}

fun processInstructions(input: String) {
    val registers = HashMap<String, Int>()
    val instructions = input.replace("\r", "").split("\n")
    var largestValueEver = 0

    for (instruction in instructions) {
        val instructionInfo = instruction.split(" ")
        val register = instructionInfo[0]
        val increment = instructionInfo[1] == "inc"
        val amount = instructionInfo[2].toInt()

        val otherRegisterIf = instructionInfo[4]
        val otherRegisterCompareType = instructionInfo[5]
        val otherRegisterCompareAmount = instructionInfo[6].toInt()

        var doTheStuff = false
        when(otherRegisterCompareType) {
            ">" -> doTheStuff = getRegister(registers, otherRegisterIf) > otherRegisterCompareAmount
            ">=" -> doTheStuff = getRegister(registers, otherRegisterIf) >= otherRegisterCompareAmount
            "<" -> doTheStuff = getRegister(registers, otherRegisterIf) < otherRegisterCompareAmount
            "<=" -> doTheStuff = getRegister(registers, otherRegisterIf) <= otherRegisterCompareAmount
            "==" -> doTheStuff = getRegister(registers, otherRegisterIf) == otherRegisterCompareAmount
            "!=" -> doTheStuff = getRegister(registers, otherRegisterIf) != otherRegisterCompareAmount
            else -> throw Exception(otherRegisterCompareType)
        }

        if(doTheStuff){
            val current = getRegister(registers, register)
            var newValue = 0
            if(increment) {
                newValue = current + amount
            } else {
                newValue = current - amount
            }
            registers[register] = newValue
            if(newValue > largestValueEver) {
                largestValueEver = newValue
            }
        }
    }

    println("Largest value in register: ${registers.values.max()}")
    println("Largest value ever: ${largestValueEver}")
}

fun getRegister(registers: HashMap<String, Int>, register: String) : Int {
    return registers[register] ?: 0
}