import java.time.Duration
import java.time.temporal.TemporalUnit
import kotlin.math.absoluteValue

val example =
"""0
3
0
1
-3"""

fun main(args: Array<String>) {
    calcSteps(example)

    val input = readFile("day5input.txt")

    var start = System.currentTimeMillis()
    calcSteps(input)
    var end = System.currentTimeMillis()
    var duration = Duration.ofMillis(end-start)
    println("P1 Duration: $duration")

    calcStepsPart2(example)
    start = System.currentTimeMillis()
    calcStepsPart2(input)
    end = System.currentTimeMillis()
    duration = Duration.ofMillis(end-start)
    println("Duration: $duration")
}

fun calcSteps(input: String) {

    val instructions = input
            .split("\n")
            .map { it.trim().trim('\r').toInt() }
            .toIntArray()

    var index = 0
    var steps = 0

    while (index >= 0 && index < instructions.size) {

        val instruction = instructions[index]
        instructions[index]++
        index += instruction
        steps++
    }

    println("Steps to complete: $steps")

}

fun calcStepsPart2(input: String) {

    val instructions = input
            .split("\n")
            .map { it.trim().trim('\r').toInt() }
            .toIntArray()

    var index = 0
    var steps = 0

    while (index >= 0 && index < instructions.size) {

        val instruction = instructions[index]
        if(instruction >= 3) {
            instructions[index]--
        }
        else {
            instructions[index]++
        }
        index += instruction
        steps++
    }


    println("Steps to complete: $steps")

}