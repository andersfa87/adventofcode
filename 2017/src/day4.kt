fun main(args: Array<String>) {
    check("aa bb cc dd ee")
    check("aa bb cc dd aa")
    check("aa bb cc dd aaa")

    val inputLines = readFile("day4input.txt").split("\r\n")

//    for (line in inputLines){
//        if(isValid(line)) {
//            println(line)
//        }
//    }

    val part1ValidCount = inputLines.count { isValid(it) }
    println("Part1 = $part1ValidCount")

    val part2ValidCount = inputLines.count { isValid2(it) }
    println("Hasher = $part2ValidCount")
}

fun check(input: String) {
    println("$input = ${isValid(input)}")
}

fun isValid(input: String) : Boolean {

    val words = input.split(" ").toList()
    val set = HashSet(words)
    return set.size == words.size

//    return input.split(" ")
//            .groupBy({ it })
//            .map { Pair(it.key, it.value.size) }
//            .all { it.second == 1 }
}


fun isValid2(input: String) : Boolean {
    val word = input.split(" ")
            .map { it.toCharArray().sortedArray() }
            .map { String(it) }
            .joinToString(" ")
    return isValid(word)
}