package day18

import java.util.*
import javax.sound.sampled.AudioSystem
import javax.sound.sampled.SourceDataLine
import javax.sound.sampled.AudioFormat
import javax.sound.sampled.LineUnavailableException




val input =
"""set i 31
set a 1
mul p 17
jgz p p
mul a 2
add i -1
jgz i -2
add a -1
set i 127
set p 316
mul p 8505
mod p a
mul p 129749
add p 12345
mod p a
set b p
mod b 10000
snd b
add i -1
jgz i -9
jgz a 3
rcv b
jgz b -1
set f 0
set i 126
rcv a
rcv b
set p a
mul p -1
add p b
jgz p 4
snd a
set a b
jgz 1 3
snd b
set f 1
add i -1
jgz i -11
snd a
jgz f -16
jgz a -19""".trim()

fun main(args: Array<String>) {

    val testInput = """set a 1
add a 2
mul a a
mod a 5
snd a
set a 0
rcv a
jgz a -1
set a 1
jgz a -2
        """.trim()

    //play(testInput.split("\n").map { it.trim() })

    val instructions = input.split("\n").map { it.trim() }

    val program1 = Program(instructions, 0)
    val program2 = Program(instructions, 1)

    var activeProgram = program1
    val otherProgram: () -> Program = { if(activeProgram == program1) program2 else program1 }

    while(!program1.terminated && !program2.terminated) {

        activeProgram.step({
            val otherProgram = otherProgram()
            if(otherProgram.sendQueue.isNotEmpty()) {
                otherProgram.sendQueue.pop()
            }
            else {
                null
            }
        })

        if(activeProgram.needsData) {
            activeProgram = otherProgram()
        }

        if(program1.needsData && program2.needsData && program1.sendQueue.isEmpty() && program2.sendQueue.isEmpty()) {
            println("Deadlock!")
            break;
        }

    }

    println("Program1 sends: ${program1.sends}")


}

class Program(val instructions: List<String>, val pValue: Long) {

    val registers = HashMap<String, Long>()
    val sendQueue = LinkedList<Long>()
    var sends = 0
    var terminated = false
    var needsData = false

    var index = 0

    init {
        registers["p"] = pValue
    }

    fun step(getFromOtherProgram: () -> Long?){

        println("Program: ${this.pValue} stepping")

        if(index < 0 || index >= instructions.size) {
            terminated = true
            return
        }

        val instruction = instructions[index]
        val parts = instruction.split(" ")

        val what = parts[0]
        val register = parts[1]

        when(what) {
            "snd" -> {
                sendQueue.addLast(registers[register] ?: 0)
                sends++
            }
            "set" -> {
                var value = parts[2].toLongOrNull()
                if(value == null) {
                    value = registers[parts[2]] ?: 0
                }
                registers[register] = value
            }
            "add" -> {
                var value = parts[2].toLongOrNull()
                if(value == null) {
                    value = registers[parts[2]] ?: 0
                }
                registers[register] = (registers[register] ?: 0) + value
            }
            "mul" -> {

                var value = parts[2].toLongOrNull()
                if(value == null) {
                    value = registers[parts[2]] ?: 0
                }

                registers[register] = (registers[register] ?: 0) * value
            }
            "mod" -> {
                var value = parts[2].toLongOrNull()
                if(value == null) {
                    value = registers[parts[2]] ?: 0
                }
                registers[register] = (registers[register] ?: 0) % value
            }
            "rcv" -> {
                val value = getFromOtherProgram()
                if(value != null) {
                    registers[register] = value
                    needsData = false
                } else {
                    needsData = true
                    return
                }
            }
            "jgz" -> {
                if(registers[register] ?: 0 > 0) {
                    var value = parts[2].toIntOrNull()
                    if(value == null) {
                        value = (registers[parts[2]] ?: 0L).toInt()
                    }
                    index += value
                    return
                }
            }
        }
        index++


        println("Program: ${this.pValue} sendQueue size: ${this.sendQueue.size}")
    }
}



object Sound {
    var SAMPLE_RATE = 8000f

    @Throws(LineUnavailableException::class)
    @JvmOverloads
    fun tone(hz: Long, msecs: Int, vol: Double = 1.0) {
        val buf = ByteArray(1)
        val af = AudioFormat(SAMPLE_RATE, 8, 1, true, false)
        val sdl = AudioSystem.getSourceDataLine(af)
        sdl.open(af)
        sdl.start()
        for (i in 0 until msecs * 8) {
            val angle = (i / (SAMPLE_RATE / hz)).toDouble() * 2.0 * Math.PI
            buf[0] = (Math.sin(angle) * 127.0 * vol).toByte()
            sdl.write(buf, 0, 1)
        }
        sdl.drain()
        sdl.stop()
        sdl.close()
    }

    @Throws(Exception::class)
    @JvmStatic
    fun main(args: Array<String>) {
        Sound.tone(15000, 1000)
    }
}