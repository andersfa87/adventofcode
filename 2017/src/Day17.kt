package day17

import java.time.Duration
import java.util.*

fun main(args: Array<String>) {


    //spinLock(3)


    //spinLock(328)
    spinLock2(328)

}

fun spinLock(input: Int) {

    val data = ArrayList<Int>(2018)
    data.add(0)

    var position = 0

    for (i in 1..2017) {

        position = ((position + input) % data.size) + 1


        data.add(position, i)

    }

    println(data)
    val indexOf2017 = data.indexOf(2017)
    println("2017 is at index: $indexOf2017. Value after that is: ${data[indexOf2017+1]}")

}

fun spinLock2(input: Int) {

    val startNode = CircularLinkedItem(0)
    var currentNode = startNode

    val start = System.currentTimeMillis()
    for (i in 1..50000000) {

        for(j in 0 until input) {
            currentNode = currentNode.next
        }

        //insert new node after current

        currentNode = currentNode.insertAfter(i)

        if(i % 100000 == 0) {

            println("Elapsed: ${Duration.ofMillis(System.currentTimeMillis() - start)}. Iterations completed: ${i}")

        }

    }

    println("Value after that is: ${startNode.next.value}")


}

class CircularLinkedItem<T>(val value: T) {

    var next: CircularLinkedItem<T>
    var previous: CircularLinkedItem<T>

    constructor(next: CircularLinkedItem<T>, previous: CircularLinkedItem<T>, value: T):this(value) {
        this.next = next
        this.previous = previous
    }

    init {
        this.next = this
        this.previous = this
    }

    fun insertAfter(value: T): CircularLinkedItem<T> {
        val currentNext = this.next
        val newNext = CircularLinkedItem(currentNext, this, value)
        currentNext.previous = newNext
        this.next = newNext

        return newNext
    }
}